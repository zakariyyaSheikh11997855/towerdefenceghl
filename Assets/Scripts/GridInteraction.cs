﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GridInteraction : MonoBehaviour {

	GameObject wireBox;
	Grid grid;
	public Material wireBoxMat;
	RaycastHit hit;

	GameObject currentBox;
	GridNode currentNode;
	public bool gridInteractionEnabled;
	public enum selectedTowerTypeEnum{ trigun, etc }
	public selectedTowerTypeEnum selectedTowerType = selectedTowerTypeEnum.trigun;
	public List<GameObject> towers;
	// Use this for initialization
	void Start () {
		grid = GetComponent<Grid> ();
		gridInteractionEnabled = false;
	}

	// Update is called once per frame
	void Update () {
		if (Input.anyKeyDown) {
			GetInput ();
		}
		if (gridInteractionEnabled) {
			var mainCamera = FindCamera ();
			hit = new RaycastHit (); 
			// We need to actually hit an object
			if (
				!Physics.Raycast (mainCamera.ScreenPointToRay (Input.mousePosition).origin,
					mainCamera.ScreenPointToRay (Input.mousePosition).direction, out hit, 4,
					Physics.DefaultRaycastLayers)) {
				Destroy (currentBox);
				Destroy (wireBox);
				return;
			}
			// Check that we are hitting a gameobject with tag Ground
			if (hit.collider.gameObject.tag != "Ground") {
				Destroy (currentBox);
				Destroy (wireBox);
				return;
			}
			//If inside grid
			if (grid.GetNode (hit.point).worldPos != new Vector3 (0, Mathf.Infinity, 0)) {
				//check if we have got to this point before
				if (currentNode != null) {
					//if we have then check if last time we got to this point we were pointing at the same node
					if (grid.GetNode (hit.point).worldPos == currentNode.worldPos) {
						//if we were return
						return;
					}
				}
				//else destroy the previous box and instantiate a new one at the new node
				Destroy (currentBox);
				Destroy (wireBox);
				currentNode = grid.GetNode (hit.point);

				Vector3 playerForward = GameObject.FindGameObjectsWithTag ("Player") [0].transform.forward;
				Debug.Log (playerForward);
				Vector3 instanDir = new Vector3(1, .2f, 1);
				if (playerForward.x > 0.1f &&playerForward.x < -0.1f) {
					instanDir.x = playerForward.x > 0.1f ? 1 : -1;
				} 
				if (playerForward.z > 0.1f &&playerForward.z < -0.1f) {
					instanDir.z = playerForward.z > 0.1 ? 1 : -1;
				}

				Vector3 instanPos = currentNode.worldPos + grid.nodeRadius * instanDir + new Vector3(0, 0.5f, 0);
				wireBox = GameObject.CreatePrimitive (PrimitiveType.Cube);
				wireBox.transform.position = instanPos;
				wireBox.transform.localScale = new Vector3 (grid.nodeRadius * 4, .3f, grid.nodeRadius * 4);
				wireBox.GetComponent<MeshRenderer> ().material = wireBoxMat;
				wireBox.GetComponent<BoxCollider> ().enabled = false;
			}
		}
	}
	private Camera FindCamera()
	{
		if (GetComponent<Camera>())
		{
			return GetComponent<Camera>();
		}

		return Camera.main;
	}
	private void GetInput(){
		if (Input.GetKeyDown (KeyCode.I)) {
			gridInteractionEnabled = !gridInteractionEnabled;
			Destroy (currentBox);
			Destroy (wireBox);
		}
		if (Input.GetKeyDown ("1")) {
			selectedTowerType = selectedTowerTypeEnum.trigun;
		} else if (Input.GetKeyDown ("2")) {
			selectedTowerType = selectedTowerTypeEnum.etc;
		}
		if (Input.GetMouseButtonDown (0)) {
			if (gridInteractionEnabled) {
				Debug.Log ("do");
				if (selectedTowerType == selectedTowerTypeEnum.trigun) {

					Vector3 playerForward = GameObject.FindGameObjectsWithTag ("Player") [0].transform.forward;
					Debug.Log (playerForward);
					Vector3 instanDir = new Vector3 (1, .2f, 1);
					if (playerForward.x > 0.1f && playerForward.x < -0.1f) {
						instanDir.x = playerForward.x > 0.1f ? 1 : -1;
					} 
					if (playerForward.z > 0.1f && playerForward.z < -0.1f) {
						instanDir.z = playerForward.z > 0.1 ? 1 : -1;
					}

					Vector3 instanPos = currentNode.worldPos + grid.nodeRadius * instanDir + new Vector3 (0, 0, 0);
					List<Vector2> towerNodes = new List<Vector2> ();
					Vector2 gridNode = grid.GetVector2 (currentNode.worldPos);
					towerNodes.Add (gridNode);
					towerNodes.Add (new Vector2(gridNode.x + instanDir.x, gridNode.y + instanDir.z));
					towerNodes.Add (new Vector2(gridNode.x + instanDir.x, gridNode.y));
					towerNodes.Add (new Vector2(gridNode.x, gridNode.y + instanDir.z));
					bool canBuildHere = true;
					foreach (Vector2 tn in towerNodes) {
						if (!grid.grid [(int)tn.x, (int)tn.y].walkable) {
							canBuildHere = false;
							return;
						}
					}
					if (canBuildHere) {
						GameObject.Instantiate (towers [0], instanPos, Quaternion.identity);
						foreach (Vector2 tn in towerNodes) {
							grid.grid [(int)tn.x, (int)tn.y].walkable = false;
						}
					}	
				}
			}
		}
	}
}
