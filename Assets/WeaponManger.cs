﻿using UnityEngine;
using UnityEngine.Networking;

public class WeaponManger : NetworkBehaviour {

	[SerializeField]
	private Transform GunHolder; 

	[SerializeField]
	private BaseWeapon primaryWeapon; 

	private BaseWeapon currentWeapon;

	//[SerializeField]
	//private GameObject weaponGFX; 
	[SerializeField]
	private string weaponLayerName = "Weapon"; // Other guns could be hidden in other layers


	void Start () {
		//at the moment, this means that we start with a weapon equipteded 
		EquipWeapon (primaryWeapon);
	}

	void EquipWeapon(BaseWeapon _weapon)
	{
		currentWeapon = _weapon;

		GameObject WeaponInstance = (GameObject)Instantiate (_weapon.graphics, GunHolder.position, GunHolder.rotation);
		WeaponInstance.transform.SetParent (GunHolder); //this is so it will follow it and move around 
	}

	//A getter for the wepaon
	public BaseWeapon GetCurrentWeapon()
	{
		return currentWeapon;
	}
	

}
